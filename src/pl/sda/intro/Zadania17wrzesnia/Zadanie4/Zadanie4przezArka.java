package pl.sda.intro.Zadania17wrzesnia.Zadanie4;

import java.util.Scanner;
import java.util.TreeSet;

public class Zadanie4przezArka {
    public static void main(String[] args) {
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj ilosc dzielnikow do mieszczenia w tablicy: ");
        int ilosc = odczyt.nextInt();
        System.out.printf("Podaj %d dzielniki",ilosc);
        int[] dzielniki = new int[ilosc];
        wypelnijdzielniki(dzielniki);
        System.out.println("Podaj liczbe calkowita: ");
        int liczba = odczyt.nextInt();
        int[] tabela = new int[liczba];
        wypelnijtabele(tabela);
        wypisz(tabela,dzielniki);
        wypiszSzczegolowo(tabela,dzielniki);
    }

    private static void wypiszSzczegolowo(int[] tabela, int[] dzielniki) {
        for(int x : dzielniki){
            System.out.printf("\nLiczby podzielne przez %d: ", x);
            for(int y: tabela){
                if(y%x==0 && y!=0) System.out.print(y + " ");
            }
        }
    }

    private static void wypisz(int[] tabela, int[] dzielniki) {
        TreeSet<Integer> tree = new TreeSet<>();
        System.out.print("Liczby podzielne: ");
        for(int x : dzielniki){
            for(int y: tabela){
                if(y%x==0 && y!=0) tree.add(y);
            }
        }
        for(int x: tree){
            System.out.print(x + " ");
        }
    }

    private static int[] wypelnijtabele(int[] tabela) {
        for(int i = 0; i < tabela.length; i++)
        {
            tabela[i] = i;
        }
        return tabela;
    }

    private static int[] wypelnijdzielniki(int[] dzielniki) {
        Scanner odczyt = new Scanner(System.in);
        for(int i = 0; i < dzielniki.length; i++)
        {
            dzielniki[i] = odczyt.nextInt();
        }
        return dzielniki;
    }
}

