package pl.sda.intro.Zadania17wrzesnia.Zadanie6;

import java.util.Scanner;
// anty Echo
public class Zadanie6przezArka {
    public static void main(String[] args) {
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj tekst do odwrocenia: ");
        String tekst = odczyt.nextLine();
        String[] arr = tekst.split("");
        odwroc(arr);
    }

    private static void odwroc(String[] arr) {
        System.out.println("Tekst odwrocony");
        for(int i = arr.length-1; i >= 0; i--)
        {
            System.out.print(arr[i]);
        }
    }
}

