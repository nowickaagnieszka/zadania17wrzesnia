package pl.sda.intro.Zadania17wrzesnia.Zadanie7;

import java.util.Scanner;

// zliczanie Znakow
public class Zadanie7przezArek {
    public static void main(String[] args) {
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj zdanie do zliczenia: ");
        String zdanie = odczyt.nextLine();
        char[] arr = zdanie.toCharArray();
        zlicz(arr);
    }

    private static void zlicz(char[] arr) {
        int litery = 0;
        int spacje = 0;
        int znaki = 0;
        int liczba = 0;
        for(char x : arr){
            if(x >= 48 && x <= 57) liczba++;
            else if((x >= 65 && x <= 90) ||(x >= 97 && x <= 122)) litery++;
            else if(x==32) spacje++;
            else znaki++;
        }
        System.out.println("Zdanie zawiera: ");
        System.out.printf("%d litery",litery);
        System.out.printf("\n%d liczby",liczba);
        System.out.printf("\n%d spacje",spacje);
        System.out.printf("\n%d znaki",znaki);

    }
}

