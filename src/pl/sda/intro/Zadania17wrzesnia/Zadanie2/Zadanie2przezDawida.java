package pl.sda.intro.Zadania17wrzesnia.Zadanie2;

import java.util.Scanner;

public class Zadanie2przezDawida {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile ma być elementów w tablicy");
        int x = scanner.nextInt();
        int[] arrayOfInt = new int[x];
        System.out.println("Podaj jakie liczby chcesz dodac do tablicy");
        for (int i = 0; i < arrayOfInt.length; i++)
        {
            arrayOfInt[i] = scanner.nextInt();
        }

        System.out.println(biggestValue(arrayOfInt));
        System.out.println(smallestValue(arrayOfInt));
        System.out.println(sumValue(arrayOfInt));
        System.out.println(averageValue(arrayOfInt));
        System.out.println(medianValue(arrayOfInt));


    }
    // podpunkt e
    private static int medianValue(int[] arrayOfInt) {
        int middleOfMedian = arrayOfInt.length / 2;
        int median = 0;
        if(arrayOfInt.length%2 == 0 )
        {
            int medianI = arrayOfInt[middleOfMedian];
            int medianJ = arrayOfInt[middleOfMedian - 1];
            median = (medianI + medianJ) / 2;
            return median;
        }
        else
        {
            median = arrayOfInt[middleOfMedian + 1];
            return median;
        }

    }
    // podpunkt d
    private static int averageValue(int[] arrayOfInt)
    {
        int value = arrayOfInt[0];
        for (int i = 1; i < arrayOfInt.length; i++)
        {
            value = value + arrayOfInt[i];
        }
        return value/arrayOfInt.length;
    }

    // podpunkt c
    private static int sumValue(int[] arrayOfInt)
    {
        int value = arrayOfInt[0];
        for (int i = 1; i < arrayOfInt.length; i++)
        {
            value = value + arrayOfInt[i];
        }
        return value;
    }
    // podpunkt b
    private static int smallestValue(int[] arrayOfInt) {
        int value = arrayOfInt[0];
        for (int i = 1; i < arrayOfInt.length; i++)
        {
            if (arrayOfInt[i] < value)
            {
                value = arrayOfInt[i];
            }
        }
        return value;

    }
    // podpunkt a
    private static int biggestValue(int[] arrayOfInt) {
        int value = arrayOfInt[0];
        for (int i = 1; i < arrayOfInt.length; i++)
        {
            if (arrayOfInt[i] > value)
            {
                value = arrayOfInt[i];
            }
        }
        return value;
    }
}

