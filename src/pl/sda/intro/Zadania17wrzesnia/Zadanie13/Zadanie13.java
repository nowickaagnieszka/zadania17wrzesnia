package pl.sda.intro.Zadania17wrzesnia.Zadanie13;

import java.util.Scanner;

//rysowowanie trojkatow
public class Zadanie13 {
    public static void main(String[] args) {
     //a)
        System.out.println("a)");
     for(int i=1; i<=8; i++){ // i wiersze
        for(int j=1; j<=i; j++){ // j to pozycja w wierszu/kolumny
            System.out.print("#");
        }
         System.out.print("\n");
     }

     //b)
        System.out.println("b)");
        for(int i=1; i<=8; i++){
            for(int j=8; j>=i; j--){
                System.out.print("#");
            }
            System.out.print("\n");
        }


     //c}
        System.out.println("c)");
        for(int i=1; i<=8; i++){
            for(int j=1; j<=i-1; j++) {
                System.out.print(" ");
            }
            for(int j=8; j>=i; j--){
                System.out.print("#");
            }
            System.out.print("\n");
        }


     //d)
        System.out.println("d)");
        for(int i=1; i<=8; i++){
            for(int j=7; j>=i; j--){
                System.out.print(" ");
            }
            for(int j=1; j<=i; j++){
                System.out.print("#");
            }
            System.out.print("\n");
        }

    }

}
