package pl.sda.intro.Zadania17wrzesnia.Zadanie3;

import java.util.Scanner;

public class Zadanie3przezDawida {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj jakas liczbe");
        int insertedNumber = scanner.nextInt();

        // Tablica liczb mniejszych od podanej

        int[] smallerNumbers = returnArray(insertedNumber);

        // komentarz do sprawdzenia czy dobrze sie wypisuje

        //        for (int i =0; i<smallerNumbers.length; i++) {
        //    System.out.println(smallerNumbers[i]);
        //}
        System.out.println();
        printDividableByThree(smallerNumbers);
        System.out.println();
        printDividableByTwo(smallerNumbers);
        System.out.println();
        System.out.println("Podaj liczbe z ktorej chcesz sprawdzic dzielniki");
        int whatYouWant = scanner.nextInt();
        printDividableByWhatYouWant(smallerNumbers, whatYouWant);
    }

    // Metoda drukuje podzielne przez 3
    private static void printDividableByThree(int[] smallerNumbers) {
        for (int i = 0; i < smallerNumbers.length; i++) {
            if (smallerNumbers[i] % 3 == 0) {
                System.out.println(smallerNumbers[i]);
            }
        }
    }
    // Metoda drukuje podzielne przez 2
    private static void printDividableByTwo(int[] smallerNumbers) {
        for (int i = 0; i < smallerNumbers.length; i++) {
            if (smallerNumbers[i] % 2 == 0) {
                System.out.println(smallerNumbers[i]);
            }
        }
    }
    // Metoda drukuje podzielne przez co chcesz
    private static void printDividableByWhatYouWant(int[] smallerNumbers, int whatYouWant) {
        for (int i = 0; i < smallerNumbers.length; i++) {
            if (smallerNumbers[i] % whatYouWant == 0) {
                System.out.println(smallerNumbers[i]);
            }
        }
    }

    // Metoda zwracajaca tablice int z liczb mniejszych od podanej
    public static int[] returnArray(int insertedNumber)
    {
        int[] array = new int[insertedNumber - 1];
        for (int i = 0; i < insertedNumber - 1; i++)
        {
            array[i] = i + 1;
        }
        return array;
    }


}

