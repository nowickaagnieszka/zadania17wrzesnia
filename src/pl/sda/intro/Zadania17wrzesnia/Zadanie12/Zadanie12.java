package pl.sda.intro.Zadania17wrzesnia.Zadanie12;

import java.util.Scanner;

// program przyjmuje od uzyt liczbe <1;20> i rysuje z nich trojkat
public class Zadanie12 {
    public static void main(String[] args) {
        System.out.println("Podaj liczbe w zakresie od 1 do 20.");
        Scanner scanner = new Scanner(System.in);
        int odczyt = scanner.nextInt();
        for(int i=1; i<=odczyt; i++) {
            for(int j=1; j<=i; j++){
               System.out.print(j);

            }
            System.out.print("\n");
        }
    }
}
