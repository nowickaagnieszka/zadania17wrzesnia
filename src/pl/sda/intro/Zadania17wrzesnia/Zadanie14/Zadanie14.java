package pl.sda.intro.Zadania17wrzesnia.Zadanie14;

import java.util.Random;
import java.util.Scanner;

public class Zadanie14 {
    public static void main(String[] args) {
        int ilosc = 0;
        double cena = 4.38;
        Random random = new Random();

        while(true) {
            System.out.println("Czy chcesz kontynuowac tankowanie [t/n]?");
            Scanner scanner = new Scanner(System.in);
            String odczyt = scanner.nextLine();


            if (odczyt.contains("t")) { //metoda contains sprawdza czy w odczyt jest zawarty string t
                int iloscDotankowana = random.nextInt(10);
                ilosc += iloscDotankowana;
                System.out.println("Zatankowano: " + iloscDotankowana + " l");
                System.out.println("Do rachunku dodano: " + (cena * (double) iloscDotankowana));

            } else  if (odczyt.contains("n")) {
                System.out.println("Zatankowano w sumie: " + ilosc +" l");
                System.out.println("Do zaplaty: " + (cena * (double) ilosc));
                break;
            } else {
                System.out.println("wybierz t lub n");
            }

        }
    }
}
