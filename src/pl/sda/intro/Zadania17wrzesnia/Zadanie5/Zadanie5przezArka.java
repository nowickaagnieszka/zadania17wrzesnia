package pl.sda.intro.Zadania17wrzesnia.Zadanie5;

import java.util.Scanner;
// suma Liczb
public class Zadanie5przezArka {
    public static void main(String[] args) {
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj liczbe: ");
        int value = odczyt.nextInt();
        String liczba = Integer.toString(value);
        String[] arr = liczba.split("");
        System.out.println(oblicz(arr));

    }

    private static int oblicz(String[] arr) {
        int suma = 0;
        for(int i = 0; i < arr.length; i++){
            suma+=Integer.parseInt(arr[i]);
        }
        return suma;
    }
}

