package pl.sda.intro.Zadania17wrzesnia.ZadaniaDoDomu;
// Rozbudowa klasy Kalkulator o modulo i zmiany sposobu wyswietlania wynikow
import java.util.Scanner;

public class Zad1 {
            public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Podaj rodzaj działania:");
            String dzialanie = scanner.nextLine();
            System.out.println("Podaj liczba1");
            int liczba1 = scanner.nextInt();
            System.out.println("Podaj liczba2");
            int liczba2 = scanner.nextInt();

            int wynik = 0;
            switch (dzialanie) {
                case "+":
                    wynik = dodaj(liczba1, liczba2);
                    break;
                case "/":
                    wynik = podziel(liczba1, liczba2);
                    break;
                case "*":
                    wynik = pomnoz(liczba1, liczba2);
                    break;
                case "-":
                    wynik = odejmij(liczba1, liczba2);
                    break;
                case "%":
                    wynik = modulo(liczba1, liczba2);
                    break;
                default:
                    System.out.println("Wybrano nieprawidłowe działanie!");
                    break;
            }
            System.out.printf("%d %s %d = %d", liczba1, dzialanie, liczba2, wynik);
        }

    private static int odejmij(int a, int b) {

                return a - b;
    }

    private static int pomnoz(int a, int b) {

                return a * b;
    }

    private static int podziel(int a, int b) {
            if (b == 0)
                throw new RuntimeException("Nie dziel przez zero!!!");
            else {
                return a / b;
            }
        }
    private static int modulo(int a, int b) {

                return a % b;

        }

        private static int dodaj(int a, int b) {
            return a + b;
        }
    }

