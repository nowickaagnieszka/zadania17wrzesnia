package pl.sda.intro.Zadania17wrzesnia.Zadanie16;

import java.util.Scanner;

public class Zadanie16 {
    public static void main(String[] args) {
        System.out.println("Wybierz czy grasz X czy O:");
        Scanner scanner = new Scanner(System.in);
        String zczytanyZnak = scanner.nextLine(); // wybrany przez uzytk, a niewybrany bedzie komputera
        String znakKomputera;
        String[][] plansza = new String [3][3];
        plansza = inicjalizacjaPlanszy(plansza);
        wypisaniePlanszy(plansza);

        if(zczytanyZnak.contains("X")){
            znakKomputera = "O";

        }else if (zczytanyZnak.contains("O")){
            znakKomputera = "X";

        } else {
            System.out.println("Nawet znaku nie potrafisz wybrac, nie chce mi sie z Toba grac...");
            return;
        }



    }
    private static void wypisaniePlanszy(String[][] plansza){
        for(int i = 0; i<3; i++)
        {
            for(int j = 0; j<3; j++) {
                System.out.print(plansza[i][j]+"|");
            }
            if(i!=2)System.out.print("\b\n-|-|-\n");
            else System.out.print("\b");
        }

    }
    private static String[][] inicjalizacjaPlanszy(String[][] plansza){
        int temp;
        for(int i = 0; i<3; i++)
        {
            for(int j = 0; j<3; j++) {
            temp=i*3+j;
                plansza[i][j] = Integer.toString(temp);
            }
        }
        return plansza;
    }

    private static String[][] wpiszRuch(String[][] plansza, int ruch, String znak){
        int temp;
        for(int i = 0; i<3; i++)
        {
            for(int j = 0; j<3; j++) {
                temp=i*3+j;
                if(temp == ruch && !plansza[i][j].contains("O") && !plansza[i][j].contains("X")) {
                    plansza[i][j] = znak;
                } else if(temp == ruch && (plansza[i][j].contains("O") || plansza[i][j].contains("X"))) {
                    System.out.println("Pole zajete, tracisz ruch!");
                }
            }
        }
        return plansza;

    }


}
