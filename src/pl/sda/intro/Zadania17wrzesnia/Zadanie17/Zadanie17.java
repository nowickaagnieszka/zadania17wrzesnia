package pl.sda.intro.Zadania17wrzesnia.Zadanie17;

import java.util.Scanner;

/*
  program, który weryfikuje czy podany PESEL jest prawdziwy
  –skorzystaj z definicji na stronie http://pl.spoj.com/problems/JPESEL/
 */
  public class Zadanie17 {
    public static void main(String[] args) {
      System.out.println("Wpisz pesel");
      Scanner scanner = new Scanner(System.in);
      String PESEL = scanner.nextLine();
      if (PESEL.length()!=11){
        System.out.println("Pesel powinien miec 11 cyfr");
        return; // wyjście z programu
      }
      char[] listaCyfr = PESEL.toCharArray(); //przypisuje Stringa na tablice char'ow (poj.znakow)

      int[] pesel  = new int[11];

      for(int i=0; i<11; i++){
        pesel[i] = (int)listaCyfr[i] - 48; // zamiana tablica char(48 => 0) na int'y (cyfry/liczby),
        //System.out.println(pesel[i]);
      }

      int suma = pesel[0]*1 + pesel[1]*3 + pesel[2]*7 + pesel[3]*9
              + pesel[4]*1 + pesel[5]*3 + pesel[6]*7 + pesel[7]*9 + pesel[8]*1 + pesel[9]*3 + pesel[10]*1;
      // System.out.println(suma);
      if (suma%10 ==0 ) { // pesel jest prawidlowy, jesli reszta z dzielenia powyzszej sumy wyniesie 0
        System.out.println("Pesel prawidłowy.");
      }else {
        System.out.println("Pesel nieprawidłowy.");

      }


    }

}
