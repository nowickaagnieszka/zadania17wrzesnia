package pl.sda.intro.Zadania17wrzesnia.Zadanie11;

// sposob bardziej elegancki
public class Zadanie11wykon2 {
    public static void main(String[] args) {
        int[] tablica1 = new int[]{1, 2, 3, 4};
        int[] tablica2 = new int[]{2, 3, 4, 5, 6};

        int[] tabTymczasowa = new int[tablica1.length];
        int indexTab3 = 0;
        for (int i = 0; i < tablica1.length; i++) {
            for (int j = 0; j < tablica2.length; j++) {
                if (tablica1[i] == tablica2[j]) {
                    tabTymczasowa[indexTab3] = tablica1[i];
                    indexTab3++;

                }
            }
        }
        // a teraz przepisujemy elementy do tablicy 3, gdyz znamy już jaka ma miec długość i dopiero teraz możemy ja zdefiniować
        int[] tablica3 = new int[indexTab3];
        for (int i = 0; i < indexTab3; i++) {
            tablica3[i] = tabTymczasowa[i];
            System.out.println(tablica3[i]);
        }
    }
}

