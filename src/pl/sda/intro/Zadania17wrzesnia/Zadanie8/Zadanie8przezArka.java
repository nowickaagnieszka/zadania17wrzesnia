package pl.sda.intro.Zadania17wrzesnia.Zadanie8;

import java.util.Scanner;
// sortowanie
public class Zadanie8przezArka {
    public static void main(String[] args) {
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj wielkosc tablicy: ");
        int wielkosc = odczyt.nextInt();
        int[] tablica = new int[wielkosc];
        bsort(wypelnianie(tablica));
        System.out.println();
        bsortdown(tablica);


    }

    private static void bsortdown(int[] tablica) {
        System.out.println("Tablica posortowana: ");
        for( int i = 0; i < tablica.length; i++ )
        {
            for( int j = 0; j < tablica.length - 1; j++ )
            {
                if( tablica[j] < tablica[j+1] )
                    tablica[j+1] = swap( tablica[j], tablica[j]=tablica[j+1] );
            }
        }
        for (int x: tablica  ) {
            System.out.print(x + " ");
        }
    }

    private static void bsort(int[] tablica) {
        System.out.println("Tablica posortowana: ");
        for( int i = 0; i < tablica.length; i++ )
        {
            for( int j = 0; j < tablica.length - 1; j++ )
            {
                if( tablica[j] > tablica[j+1] )
                    tablica[j+1] = swap( tablica[j], tablica[j]=tablica[j+1] );
            }
        }
        for (int x: tablica  ) {
            System.out.print(x + " ");
        }
    }

    private static int swap(int i, int i1) {
        return i;
    }

    private static int[] wypelnianie(int[] tablica) {
        Scanner odczyt = new Scanner(System.in);
        for(int i = 0; i < tablica.length; i++){
            System.out.printf("Tablica[%d] = ",i);
            tablica[i] = odczyt.nextInt();
        }
        return tablica;
    }
}

