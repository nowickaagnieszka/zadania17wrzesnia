package pl.sda.intro.Zadania17wrzesnia.Zadanie1;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1przezMarcina {
            public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Podaj 6 Liczb");
            int[] liczbyGracza = new int[6];
            for (int i = 0; i < 6; i++) {
                liczbyGracza[i] = scanner.nextInt();
            }
            wyswietlTablice(liczbyGracza);
            int[] wylosowaneLiczby = new int[6];
            for (int i = 0; i < 6; i++) {
                wylosowaneLiczby[i] = losuj(49);
            }
            int[] trafioneLiczby = new int[6];
            for (int i = 0; i < 6; i++) {
                if (czyLiczbaZnajdujeSieWTablicy(liczbyGracza, wylosowaneLiczby[i])) {
                    trafioneLiczby[i] = wylosowaneLiczby[i];
                }
            }
            wyswietlTablice(wylosowaneLiczby);
            System.out.println("Trafione:");
            wyswietlTablice(trafioneLiczby);
        }

        private static boolean czyLiczbaZnajdujeSieWTablicy(int[] liczbyGracza, int liczba) {
            for (int i = 0; i < liczbyGracza.length; i++) {
                if (liczbyGracza[i] == liczba)
                    return true;
            }
            return false;
        }

        private static void wyswietlTablice(int[] liczby) {
            for (int i = 0; i < liczby.length; i++) {
                System.out.print(liczby[i] + " ");
            }
            System.out.println();
        }

        private static int losuj(int liczba) {
            Random random = new Random();
            return random.nextInt(liczba);
        }
    }

