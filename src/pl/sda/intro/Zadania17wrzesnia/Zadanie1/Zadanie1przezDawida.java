package pl.sda.intro.Zadania17wrzesnia.Zadanie1;

import java.util.Random;
import java.util.Scanner;
// losowanie lotto
public class Zadanie1przezDawida {
            public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);

            // Inicjalizacja tablic z liczbami lotto i gracza

            int[] playerNumbers = new int[6];
            int[] lottoNumbers = new int[6];
            int inserted = 0;
            int randomized = 0;


            System.out.println("Podaj 6 liczb do systemu lotto");

            for (int i = 0; i < 6; i++) {
                // Pobieranie liczby od uzytkownika

                inserted = scanner.nextInt();

                // Sprawdzanie, czy to nie duplikat
                boolean found = false;
                for (int j = 0; j < 6; j++) {
                    if (inserted == playerNumbers[j]) {
                        i--;
                        System.out.println("Wprowadz inna liczbe");
                        found = true;
                        break;
                    }

                }
                if (found == false)
                    playerNumbers[i] = inserted;

            }

            for (int i = 0; i < 6; i++) {
                // Losowanie liczby

                randomized = rand(48);

                // Sprawdzanie, czy to nie duplikat
                boolean found = false;
                for (int j = 0; j < 6; j++) {
                    if (randomized == lottoNumbers[j]) {
                        i--;
                        System.out.println("Lotto wybralo ta sama liczbe, wyszukuje ponownie");
                        found = true;
                        break;
                    }

                }
                if (found == false)
                    lottoNumbers[i] = randomized;


            }
            System.out.println("GRATULACJE TRAFILES TYLE LICZB: " + checkResult(playerNumbers, lottoNumbers));
            //for (int i = 0; i < 6; i++) {
            //   System.out.println(lottoNumbers[i]);
            //   System.out.println(playerNumbers[i]);
            //}
        }

        private static int checkResult(int[] player, int[] lotto) {
            int result = 0;
            for (int i = 0; i < 6; i++) {
                for (int j = 0; j < 6; j++) {
                    if (player[i] == lotto[j]) {
                        result++;
                        break;
                    }
                }
            }
            return result;
        }

        private static int rand(int number) {
            Random random = new Random();
            return random.nextInt(number) + 1;
        }
    }

