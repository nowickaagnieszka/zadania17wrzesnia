package pl.sda.intro.Zadania17wrzesnia.Zadanie1;

import java.util.Random;
import java.util.Scanner;

/* program, który zwraca w postaci tablicy zbiór wszystkich liczb mniejszych od zadanej przez użytkownika liczby
   oraz podzielnych przez 2, podzielnych przez 3, a dla chetnych podzielnych przez podana przez uzytk liczbe
 */
public class Zadanie1 {
    public static void main(String[] args) {
        System.out.println("Wytypuj szesc liczb, w przedziale cyfr od 1 do 49.");
        Scanner scanner = new Scanner(System.in);

        int losowaLiczba = losuj(49);
        System.out.println(losowaLiczba);
    }

    private static int losuj(int liczba) {
        Random random = new Random();
        return random.nextInt(liczba);
    }
}







