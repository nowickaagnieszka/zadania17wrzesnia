package pl.sda.intro.Zadania17wrzesnia.Zadanie10;

import java.util.Scanner;

// program, ktory przyjmie od uzytk liste liczb oddzielonych przecinkami i usunie wszystkie zduplikowane pozycje
public class Zadanie10 {
    public static void main(String[] args) {
        System.out.println("Podaj liczby, oddzielajac je przecinkiem.");
        Scanner scanner = new Scanner(System.in);
        String odczyt = scanner.nextLine();


        String[] liczby;
        liczby = odczyt.split(",");
        int dl = liczby.length;
        // dl to dlugosc tablicy, ilosc liczb podanych przez uzytkownika

        for (int i=0; i<dl; i++) {

            for (int j=i+1; j<dl; j++ ) {

                if (Integer.parseInt(liczby[i]) == Integer.parseInt(liczby[j])){
                    liczby[j] = "-1";

                }
            }
        }
        for (int i=0; i<dl; i++) {
            if(liczby[i]!="-1") {
                System.out.println(liczby[i]);
            }
        }


    }
}